# EX-admin

> 基于 【X-admin](https://gitee.com/daniuit/X-admin) 开源项目整改的， 主要做了如下功能调整：

1. 使用 Vue 2.0 对整改项目进行了重构，提高开发效率
2. 更改城市联动选择插件，使用了 [JAreaSelect](https://gitee.com/blackfox/JAreaSelect) 替代，理由是 JAreaSelect 地址数据是采集自京东商城，更加详细和精确。而且有数据的SQL存档，可以导入后台对数据进行维护。
3. 整合文件上传和编辑器, 以及分页功能
4. 更多功能后续将会陆续添加, 比如使用次后台整合一套 PHP 和 Java web 后台开发的手脚架 ...

## 预览地址

[http://exadmin.r9it.com/](http://exadmin.r9it.com/)
